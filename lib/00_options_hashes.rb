def transmogrify(text, options = {})
  defaults = {times: 1, upcase: false, reverse: false}
  opts = defaults.merge(options)

  transmogrified = text.dup
  output = []
  if opts[:upcase]
    transmogrified.upcase!
  end
  if opts[:reverse]
    transmogrified.reverse!
  end

  opts[:times].times {output << transmogrified}
  output.join
end
